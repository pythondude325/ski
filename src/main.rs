use anyhow::Context;
use crossterm::terminal::{disable_raw_mode, enable_raw_mode};

mod game;
mod output;

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    enable_raw_mode()?;

    let (output_tx, output_thread_handle) = output::create_output_thread();
    let game_thread = game::game_thread(output_tx);

    game_thread.await?;

    output_thread_handle
        .await
        .context("Waiting for output thread")?
        .context("Error in output thread")?;

    disable_raw_mode()?;
    Ok(())
}
