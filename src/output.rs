use crossterm::{cursor, QueueableCommand};
use std::io::Write;
use tokio::{sync::mpsc::UnboundedSender, task::JoinHandle};

#[derive(Clone, Debug)]
pub enum OutputEvent {
    WriteLine(String),
    WriteString(String),
    CommandMoveToColumn(cursor::MoveToColumn),
    CommandMoveToPreviousLine(cursor::MoveToPreviousLine),
    CommandMoveToNextLine(cursor::MoveToNextLine),
    Flush,
    Finish,
}

pub type OutputSender = UnboundedSender<OutputEvent>;

pub fn create_output_thread() -> (OutputSender, JoinHandle<std::io::Result<()>>) {
    let (output_tx, output_rx) = tokio::sync::mpsc::unbounded_channel();

    let output_thread = tokio::task::spawn_blocking(move || -> std::io::Result<()> {
        let mut rx = output_rx;
        let mut stdout = std::io::stdout();

        while let Some(ev) = rx.blocking_recv() {
            match ev {
                OutputEvent::WriteLine(str) => {
                    write!(&mut stdout, "{}\r\n", str)?;
                }
                OutputEvent::WriteString(str) => {
                    write!(&mut stdout, "{}", str)?;
                }
                OutputEvent::CommandMoveToColumn(command) => {
                    stdout.queue(command)?;
                }
                OutputEvent::CommandMoveToPreviousLine(command) => {
                    stdout.queue(command)?;
                }
                OutputEvent::CommandMoveToNextLine(command) => {
                    stdout.queue(command)?;
                }
                OutputEvent::Finish => {
                    rx.close();
                }
                OutputEvent::Flush => {
                    stdout.flush()?;
                }
            }
        }
        stdout.flush()?;

        Ok(())
    });

    return (output_tx, output_thread);
}
