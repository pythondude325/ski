use anyhow::Context;
use crossterm::{
    cursor,
    event::{Event, KeyCode},
};
use futures::StreamExt;
use rand::{Rng, SeedableRng};
use std::{collections::VecDeque, time::Duration};
use tokio::time::Instant;

use super::output;

const START_CLEAR_LINES: usize = 5;
const LOOKAHEAD_LINES: usize = 10;
const GAME_WIDTH: usize = 40;

fn time_formula(turn: f64) -> f64 {
    (4.0 * turn + 4.0).sqrt() - 1.5
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TurnAction {
    GoLeft,
    GoStraight,
    GoRight,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Obstacle {
    Tree,
}

fn generate_row(rng: &mut impl Rng) -> Vec<Option<Obstacle>> {
    let mut row = generate_starting_row();

    row[0] = Some(Obstacle::Tree);
    row[GAME_WIDTH - 1] = Some(Obstacle::Tree);

    let tree_pos = rng.gen_range(1..=GAME_WIDTH - 2);
    row[tree_pos] = Some(Obstacle::Tree);

    row
}

fn generate_starting_row() -> Vec<Option<Obstacle>> {
    let mut row = vec![None; GAME_WIDTH];

    row[0] = Some(Obstacle::Tree);
    row[GAME_WIDTH - 1] = Some(Obstacle::Tree);

    row
}

fn display_row(row: &[Option<Obstacle>]) -> String {
    row.into_iter()
        .map(|o| match o {
            &Some(Obstacle::Tree) => '^',
            &None => ' ',
        })
        .collect::<String>()
}

pub async fn game_thread(output_tx: output::OutputSender) -> anyhow::Result<()> {
    let game_start_time = Instant::now();

    let mut input_stream = crossterm::event::EventStream::new();
    let mut rng = rand_pcg::Pcg64::from_entropy();

    let mut y = 0;
    let mut x = GAME_WIDTH / 2;

    let mut world = VecDeque::<Vec<Option<Obstacle>>>::new();
    for _i in 0..START_CLEAR_LINES {
        world.push_back(generate_starting_row());
    }
    for _i in START_CLEAR_LINES..LOOKAHEAD_LINES {
        world.push_back(generate_row(&mut rng));
    }
    for row in &world {
        output_tx.send(output::OutputEvent::WriteLine(display_row(row)))?;
    }
    output_tx.send(output::OutputEvent::CommandMoveToPreviousLine(
        cursor::MoveToPreviousLine(LOOKAHEAD_LINES as u16),
    ))?;
    output_tx.send(output::OutputEvent::CommandMoveToColumn(
        cursor::MoveToColumn(x as u16),
    ))?;
    output_tx.send(output::OutputEvent::WriteString(String::from("i")))?;
    output_tx.send(output::OutputEvent::CommandMoveToNextLine(
        cursor::MoveToNextLine(LOOKAHEAD_LINES as u16),
    ))?;
    output_tx.send(output::OutputEvent::Flush)?;

    let mut action = TurnAction::GoStraight;

    'gameloop: loop {
        let time = game_start_time + Duration::from_secs_f64(time_formula(y as f64));
        let delay_fut = tokio::time::sleep_until(time);
        let inputs: Vec<std::io::Result<Event>> = input_stream
            .by_ref()
            .take_until(delay_fut)
            .collect::<Vec<_>>()
            .await;

        for input in inputs {
            let input = input.context("Error in input")?;
            action = if let Event::Key(ke) = input {
                match ke.code {
                    KeyCode::Esc => break 'gameloop,
                    KeyCode::Left => TurnAction::GoLeft,
                    KeyCode::Down => TurnAction::GoStraight,
                    KeyCode::Right => TurnAction::GoRight,
                    _ => action,
                }
            } else {
                action
            }
        }

        // Make a new row for the worl
        let new_row = generate_row(&mut rng);
        output_tx.send(output::OutputEvent::WriteLine(display_row(&new_row)))?;
        world.push_back(new_row);

        // Remove old row from world
        world.pop_front();

        // Erase character's last position
        output_tx.send(output::OutputEvent::CommandMoveToPreviousLine(
            cursor::MoveToPreviousLine((LOOKAHEAD_LINES + 1) as u16),
        ))?;
        output_tx.send(output::OutputEvent::CommandMoveToColumn(
            cursor::MoveToColumn(x as u16),
        ))?;
        let turn_char = match action {
            TurnAction::GoLeft => "/",
            TurnAction::GoStraight => "|",
            TurnAction::GoRight => "\\",
        };
        output_tx.send(output::OutputEvent::WriteString(String::from(turn_char)))?;

        // Calculate new position
        match action {
            TurnAction::GoLeft => x -= 1,
            TurnAction::GoStraight => {}
            TurnAction::GoRight => x += 1,
        };

        let next_row = &world[0];
        if next_row[x].is_some() {
            output_tx.send(output::OutputEvent::CommandMoveToNextLine(
                cursor::MoveToNextLine(1),
            ))?;
            output_tx.send(output::OutputEvent::CommandMoveToColumn(
                cursor::MoveToColumn(x as u16),
            ))?;
            output_tx.send(output::OutputEvent::WriteString(String::from("X")))?;
            output_tx.send(output::OutputEvent::CommandMoveToNextLine(
                cursor::MoveToNextLine(LOOKAHEAD_LINES as u16),
            ))?;
            break;
        }

        // Draw character
        output_tx.send(output::OutputEvent::CommandMoveToNextLine(
            cursor::MoveToNextLine(1),
        ))?;
        output_tx.send(output::OutputEvent::CommandMoveToColumn(
            cursor::MoveToColumn(x as u16),
        ))?;
        output_tx.send(output::OutputEvent::WriteString(String::from("i")))?;
        output_tx.send(output::OutputEvent::CommandMoveToNextLine(
            cursor::MoveToNextLine(LOOKAHEAD_LINES as u16),
        ))?;
        // Flush so that all the stuff we just sent gets drawn
        output_tx.send(output::OutputEvent::Flush)?;

        y += 1;
    }

    output_tx.send(output::OutputEvent::WriteLine(format!(
        "Game Over. Your score was {}",
        y
    )))?;
    output_tx.send(output::OutputEvent::Finish)?;

    Ok(())
}
